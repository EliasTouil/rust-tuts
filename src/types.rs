pub fn run(){

    // Default is "i32"
    let _type_i32 = 1;

    // implicit cast to f64 because floating point
    let _type_implicit_f64 = 2.5;

    // explicit i64
    let _type_i64 : i64 = 2345643567435657;

    //max sizes
    println!("Max for i32 : {}", std::i32::MAX);
    println!("Max for i64 : {}", std::i64::MAX);

    // Boolean implicit
    let _implicit_boolean = true;

    let _implicit_boolean_2 = 10 > 5;

    println!("{}", _implicit_boolean_2);

}
