// str is a primitive
// String is an array on heap
pub fn run(){

    let _hello = String::from("Hello");

    println!("String {} and  its length : {}", _hello, _hello.len());

    let mut _my_string = String::from("My super stringy string matter");
    println!("{}", _my_string);
    _my_string.push_str(".");
    println!("{}", _my_string);

    println!("{}", _my_string.is_empty());

    println!("{}", _my_string.contains("super"));

    println!("{}",  _my_string.replace("super", "duper"));

    for word in _my_string.split_whitespace() {
        println!("{}", word);
    }

    let mut _s = String::with_capacity(10);
    _s.push('a');
    _s.push('b');

    assert_eq!(2, _s.len());
    println!("{}", _s);
}
