// Arrays have a fixed lenght (NOT MAX, EQUAL)
pub fn run(){
    let mut _my_array: [usize; 5] = [1, 2, 3, 4, 5];
    println!("{:?}", _my_array);

    //re-assign a value as the memory space allocation in bytes
    _my_array[2] = std::mem::size_of_val(&_my_array);
    println!("{:?}", _my_array);

    // to get a single value
    println!("{}", _my_array[0]);
}
