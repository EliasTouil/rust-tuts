/*
Primitive datta
variables are immutable
block scoped
*/

pub fn run(){
    //immutable var
    let name = "Brad";
    //mutable var
    let mut age = 37;
    age += 1-1;
    println!("My name is {} and I am {}", name, age);

    //Define a constant
    // Must add type
    const ID: i32 = 001;
    println!("{}", ID);

    // Assiign multiple vars
    let ( my_name, my_age) = ("Brad", 37);
    println!("{}  and i'm {}", my_name, my_age);

}
