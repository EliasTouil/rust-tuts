// Tthis is what rruns the file
pub fn run(){
    println!("hello from the other faaiiille");

    // You can't just print an integer
    // Will not work : println!(1);
    // This works:
    println!("{}", 1);

    //Positional Arguments
    println!("{} is from {} and {} likes to {}", "Brad", "Mass", "Brad", "code");

    //Named arguments
    println!("{namedArg1} is so cool", namedArg1="Brad");

    // Placeholder traits
    println!("Binary: {:b} Hex:{:x} Octal:{:o}", 10, 10, 10);

    //  Placeholder for debug trait (to print arrays (and such?))
    println!("{:?}", (12, true, "Hello"));
}
